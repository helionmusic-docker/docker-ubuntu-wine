# Docker Ubuntu Wine

Docker repository for generating a Ubuntu image with Wine installed.

To be used to build other docker images with a Wine dependency

Based off of: https://github.com/scottyhardy/docker-wine/blob/master/Dockerfile 
